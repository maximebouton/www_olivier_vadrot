# Devis

Les tâches ne sont pas listées par ordre chronologique

## Développement général

| Tâches                                              | jours |
| --------------------------------------------------- | ----- |
| Choix, déploiement, configuration d'un CMS[^1]      | 3     |
| Mise à jour hebergement, migration base de données  | 3     |
| Architecture, arborescence du site[^3]              | 3     |
| Configuration 3 langues                             | 4     |
| Configuration newsletter                            | 3     |
| Mise en ligne                                       | 2     |

## Design

| Tâches                                              | jours      |
| --------------------------------------------------- | ---------- |
| Favicon                                             | CADEAU[^2] |
| Responsive                                          | 4          |
| Intégration,  adaptation graphique (main.css)       | 3          |

## Templates

Pour chaque : installation CMS (model.py) + page.html + feuille.css + script.js

| Nom original            | nom du model            | jours |
| ----------------------- | ----------------------- | ----- |
|                         | common-list[^4]         | 3     |
|                         | common-item             | 1     |
| (HOMEPAGE)              | project-list            | 1     |
| (PROJECT DETAILS)       | project-item            | 1     |
| (NEWS)                  | news-list               | 1     |
| (NEWS DETAILS)          | news-item               | 0.5   |
|                         | review-list             | 1     |
|                         |     review-item[^5]     |       |
|                         | cavea                   | 1     |
|                         | about-index             | 1     |
|                         | about-item              | 1     |
|                         | contact                 | 0.5   |

## Estimation

### Livraison

38 j / 2 personnes, 19 jours de travail.

### Proposition de calendrier

Avant 19 juillet signature du devis, dernières préçisions, accord sur maquette fixé ( sinon on doit ajuster le devis ).

Livraison d'une version fonctionelle simplifiée 1.0 au 1er septembre

Fin septembre : retour critique sur le site, usage, design, précisions des besoins

Livraison d'une version 2.0 au 1er novembre

### Coût[^6]

200€ / jour

version 1.0 :   5700 €
acompte (33%)   1881 €
facture         3819 €

version 2.0 :   1900 €
acompte (33%)   627  €
facture         1273 €

[1]: Django | Processwire | Kirby
[2]: J'adore faire des favicon : http://79.87.130.164/images/icons/index.html 
[3]: Planification, gestion des urls, navigation            
[4]: Développement général de gestions de listes potentielement triables, filtres, catégories pour projet, news, review, about etc. 
[5]: sous-item sans css/js
[6]: Les auto-entrepreneurs ne prennent pas en compte la TVA
